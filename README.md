# Simple Go App

Source: https://gitlab.com/gitpod-examples/simple-go-app

This is a simple docker with golang based http server. It is stupid simple, absolutely useless but helps to get into docker with golang

Debugging is possible but not enabled 

See: 
https://github.com/golang/vscode-go/blob/master/docs/debugging.md#connecting-to-headless-delve-with-target-specified-at-server-start-up

https://github.com/go-delve/delve

## With Dockerfile

**Build it**

    docker build -t mysimple .

**Run it detached (-d)**

    docker run -d --publish 5000:5000  mysimple
    
**View it (-d)**

http://localhost:5000 


## With DockerCompose

**Run it**

    docker-compose up
    
**View it (-d)**

http://localhost:5000 
